﻿using Xunit;
using RPG_Characters.Characters;

namespace UnitTests
{
    public class Character
    {
        [Theory]
        [InlineData(100, 15)]
        [InlineData(210, 20)]
        [InlineData(331, 25)]
        [InlineData(464, 30)]
        [InlineData(610, 35)]
        [InlineData(770, 40)]
        public void WarriorAttributesAfterXPGain(int xp, int str)
        {
            Warrior warrior = new Warrior();
            warrior.GrantXP(xp);
            Assert.Equal(str, warrior.Attributes.Strength);
        }

        [Fact]
        public void RangerAttributesAfterXPGain()
        {
            Ranger ranger = new Ranger();
            while (ranger.Level < 12) ranger.GrantXP(100);

            int[] rangerAttrs = new int[] { 340, 27, 65, 13 };
            int[] rangerAttrsActual = new int[] { ranger.Attributes.HP, ranger.Attributes.Strength, ranger.Attributes.Dexterity, ranger.Attributes.Intelligence };

            Assert.Equal(rangerAttrs, rangerAttrsActual);
        }
    }
}
