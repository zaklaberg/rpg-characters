﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Characters.Items.Armor
{
    public enum ArmorSlot
    {
        HEAD,
        BODY,
        LEGS
    }

    public enum ArmorType
    {
        CLOTH, 
        LEATHER,
        PLATE
    }

    public class Armor : Item
    {
        protected CharacterAttributes providedAttributesBase = CharacterAttributes.Fill(0);
        protected CharacterAttributes providedAttributesPerItemLevel = CharacterAttributes.Fill(0);

        public ArmorSlot Slot { get; private set; }
        public ArmorType Type { get; private set; }

        private static readonly Dictionary<ArmorSlot, double> SlotAttributeModifiers = new Dictionary<ArmorSlot, double>
        {
            { ArmorSlot.BODY, 1 },
            { ArmorSlot.HEAD, 0.8 },
            { ArmorSlot.LEGS, 0.6 }
        };

        private Armor(ArmorType type)
        {
            Type = type;
        }

        public virtual CharacterAttributes ProvidedAttributes
        {
            get
            {
                CharacterAttributes totalAttributes = providedAttributesBase + providedAttributesPerItemLevel * ItemLevel;
                double modifier = SlotAttributeModifiers[Slot];
                return totalAttributes * modifier;
            }
        }

        public static ArmorBuilder Create(ArmorType type)
        {
            return new ArmorBuilder(type);
        }

        public class ArmorBuilder
        {
            private const int NumConstructionStages = 4;
            private int currentConstructionStage = 0;
            Armor armor = null;
            public ArmorBuilder(ArmorType type)
            {
                armor = new Armor(type);
                switch (type)
                {
                    case ArmorType.CLOTH:
                        armor.providedAttributesBase.HP = 10;
                        armor.providedAttributesBase.Intelligence = 3;
                        armor.providedAttributesBase.Dexterity = 1;

                        armor.providedAttributesPerItemLevel.HP = 5;
                        armor.providedAttributesPerItemLevel.Intelligence = 2;
                        armor.providedAttributesPerItemLevel.Dexterity = 1;
                        break;
                    case ArmorType.LEATHER:
                        armor.providedAttributesBase.HP = 20;
                        armor.providedAttributesBase.Strength = 1;
                        armor.providedAttributesBase.Dexterity = 3;

                        armor.providedAttributesPerItemLevel.HP = 8;
                        armor.providedAttributesPerItemLevel.Dexterity = 2;
                        armor.providedAttributesPerItemLevel.Strength = 1;
                        break;
                    case ArmorType.PLATE:
                        armor.providedAttributesBase.HP = 30;
                        armor.providedAttributesBase.Strength = 3;
                        armor.providedAttributesBase.Dexterity = 1;

                        armor.providedAttributesPerItemLevel.HP = 12;
                        armor.providedAttributesPerItemLevel.Strength = 2;
                        armor.providedAttributesPerItemLevel.Dexterity = 1;
                        break;
                    default:
                        throw new Exception($"ArmorBuilder encountered unknown ArmorType: {type.ToString("F")}");
                }

                ++currentConstructionStage;
            }

            public ArmorBuilder Slot(ArmorSlot slot)
            {
                armor.Slot = slot;
                ++currentConstructionStage;
                return this;
            }

            public ArmorBuilder ItemLevel(int itemLevel)
            {
                armor.ItemLevel = itemLevel;
                ++currentConstructionStage;
                return this;
            }

            public ArmorBuilder Name(string name)
            {
                armor.Name = name;
                ++currentConstructionStage;
                return this;
            }

            public Armor Build()
            {
                if (currentConstructionStage != NumConstructionStages)
                    throw new Exception("Please finish constructing the armor before Build()ing.");
                return armor;
            }
        }
    }
}
