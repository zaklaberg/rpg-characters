﻿using System;
using System.Collections.Generic;
using System.Text;
using RPG_Characters.Items.Weapons;
using RPG_Characters.Characters;

namespace RPG_Characters.Abilities
{
    public class NormalAttack : IAbility
    {
        private const double StrengthScalingMultiplier = 1.5;
        private const int DexterityScalingMultiplier = 2;
        private const int IntelligenceScalingMultiplier = 3;

        public void Use(Character user, Character target)
        {
            // Damage calculation
            int damageFromAttributes = GetDamageFromAttributes(user);
            int damage = user.weapon.Damage + damageFromAttributes;

            // Effects on user / target
            target.TakeDamage(damage);
        }

        private int GetDamageFromAttributes(Character user)
        {
            if (user.weapon == null)
                return 0;


            // Different types of weapons scale of different attributes
            return user.weapon.Type switch
            {
                WeaponType.MELEE => Convert.ToInt32(Math.Floor(StrengthScalingMultiplier * user.Attributes.Strength)),
                WeaponType.RANGED => DexterityScalingMultiplier * user.Attributes.Dexterity,
                WeaponType.MAGIC => IntelligenceScalingMultiplier * user.Attributes.Intelligence,
                _ => 0
            };
        }
    }
}
