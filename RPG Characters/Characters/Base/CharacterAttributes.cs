﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Characters
{
    public class CharacterAttributes
    {
        public int Strength { get; set; } = 0;
        public int Dexterity { get; set; } = 0;
        public int Intelligence { get; set; } = 0;
        public int HP { get; set; } = 0;

        public static CharacterAttributes operator +(CharacterAttributes a, CharacterAttributes b)
        {
            CharacterAttributes res = new CharacterAttributes() 
            { 
                HP = a.HP + b.HP,
                Dexterity = a.Dexterity + b.Dexterity,
                Intelligence = a.Intelligence + b.Intelligence,
                Strength = a.Strength + b.Strength
            };

            return res;
        }

        public static CharacterAttributes operator -(CharacterAttributes a, CharacterAttributes b)
        {
            CharacterAttributes res = new CharacterAttributes()
            {
                HP = a.HP - b.HP,
                Dexterity = a.Dexterity - b.Dexterity,
                Intelligence = a.Intelligence - b.Intelligence,
                Strength = a.Strength - b.Strength
            };

            return res;
        }

        public static CharacterAttributes operator *(CharacterAttributes a, double scale)
        {
            CharacterAttributes res = new CharacterAttributes()
            {
                HP = Convert.ToInt32(Math.Floor(a.HP * scale)),
                Dexterity = Convert.ToInt32(Math.Floor(a.Dexterity * scale)),
                Intelligence = Convert.ToInt32(Math.Floor(a.Intelligence * scale)),
                Strength = Convert.ToInt32(Math.Floor(a.Strength * scale))
            };

            return res;
        }
           
        public static CharacterAttributes Fill(int val)
        {
            CharacterAttributes ca = new CharacterAttributes()
            {
                Strength = val,
                Dexterity = val,
                Intelligence = val,
                HP = val
            };

            return ca;
        }
    }
}
