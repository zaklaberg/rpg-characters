﻿using System;
using System.Collections.Generic;

#nullable disable

namespace RPG_Characters.Model
{
    public partial class Item
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
