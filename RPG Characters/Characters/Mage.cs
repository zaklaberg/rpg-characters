﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Characters.Characters
{
    public class Mage : Character
    {
        public Mage() 
        {
            baseAttributes.HP = 100;
            baseAttributes.Strength = 2;
            baseAttributes.Dexterity = 3;
            baseAttributes.Intelligence = 10;
        }

        public override void OnLevelUp()
        {
            baseAttributes.HP += 15;
            baseAttributes.Strength += 1;
            baseAttributes.Dexterity += 2;
            baseAttributes.Intelligence += 5;
        }
    }
}
