﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Characters.Characters
{
    public class Ranger : Character
    {
        public Ranger()
        {
            baseAttributes.HP = 120;
            baseAttributes.Strength = 5;
            baseAttributes.Dexterity = 10;
            baseAttributes.Intelligence = 2;
        }

        public override void OnLevelUp()
        {
            baseAttributes.HP += 20;
            baseAttributes.Strength += 2;
            baseAttributes.Dexterity += 5;
            baseAttributes.Intelligence += 1;
        }
    }
    
}
