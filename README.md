# RPG Characters

A carefully crafted application that lets you 
- create your very own characters
- give them items
- (manually) level them up
- ... and so much more!

## Usage
You need... well. Visual Studio to build the solution, and that's about it.

### For the intrepid developer
If you wish to extend this fantastic project, there are some things you should know:
- To create a new character class(e.g. a Mathematician with zero strength gain and 5000 intellect), you only need to extend the `Character` base class, set base attributes in constructor and override `OnLevelUp`.
- To add new weapons/armor, use the corresponding builder classes. To add new weapon/armor _types_, the builder classes must be modified, and abilities that scale off types must also be updated.
- To add new abilities, implement `IAbility`, then just add to whichever `Character` you wish.
