using Xunit;
using RPG_Characters.Items.Weapons;
using RPG_Characters.Items.Armor;
using RPG_Characters.Characters;

namespace IntegrationTests
{
    public class EquipItems
    {
        [Fact]
        public void WarriorWithItems()
        {
            Warrior warrior = new Warrior();
            while (warrior.Level < 9) warrior.GrantXP(30);

            Weapon greatAxe = Weapon.Create(WeaponType.MELEE)
                .ItemLevel(5)
                .Name("Great Axe of the Exiled")
                .Build();

            Armor armor = Armor.Create(ArmorType.PLATE)
                .ItemLevel(5)
                .Slot(ArmorSlot.BODY)
                .Name("Plate Chest of the Juggernaut")
                .Build();

            warrior.Equip(greatAxe);
            warrior.Equip(armor);

            int[] expectedAttrs = new int[] { 480, 63, 25, 9 }; // HP, str, dex, int
            int[] actualAttrs = new int[] { warrior.Attributes.HP, warrior.Attributes.Strength, warrior.Attributes.Dexterity, warrior.Attributes.Intelligence };

            Assert.Equal(expectedAttrs, actualAttrs);
        }
    }
}
