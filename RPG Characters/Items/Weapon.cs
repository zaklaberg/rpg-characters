﻿using System;
using System.Collections.Generic;
using System.Text;

using RPG_Characters.Items;
using RPG_Characters.Items.Weapons;

namespace RPG_Characters.Items.Weapons
{
    public enum WeaponType
    {
        MELEE,
        RANGED,
        MAGIC
    }

    public class Weapon : Item
    {
        public int Damage 
        {
            get
            {
                return baseDamage  + ItemLevel * baseDamageLevelMultiplier;
            }
        }

        private int baseDamage = 0;
        private int baseDamageLevelMultiplier = 0;

        public WeaponType Type { get; private set; }

        private Weapon(WeaponType type)
        {
            Type = type;
        }

        public static WeaponBuilder Create(WeaponType type)
        {
            return new WeaponBuilder(type);
        }


        public class WeaponBuilder
        {
            private const int NumConstructionStages = 3;
            private int currentConstructionStage = 0;
            private Weapon weapon = null;
            public WeaponBuilder(WeaponType type)
            {
                weapon = new Weapon(type);
                switch(type)
                {
                    case WeaponType.MELEE:
                        weapon.baseDamage = 15;
                        weapon.baseDamageLevelMultiplier = 2;
                        break;
                    case WeaponType.RANGED:
                        weapon.baseDamage = 5;
                        weapon.baseDamageLevelMultiplier = 3;
                        break;
                    case WeaponType.MAGIC:
                        weapon.baseDamage = 25;
                        weapon.baseDamageLevelMultiplier = 2;
                        break;
                    default:
                        throw new Exception("WeaponBuilder encountered unknown weapon type.");
                }

                ++currentConstructionStage;
            }

            public WeaponBuilder ItemLevel(int iLevel)
            {
                weapon.ItemLevel = iLevel;
                ++currentConstructionStage;
                return this;
            }

            public WeaponBuilder Name(string name)
            {
                weapon.Name = name;
                ++currentConstructionStage;
                return this;
            }

            public Weapon Build()
            {
                if (currentConstructionStage != NumConstructionStages)
                    throw new Exception("Your weapon is incompete. Please finish constructing before Build()ing.");

                Weapon wout = weapon;
                weapon = null;
                return wout;
            }
        }
    }
}
