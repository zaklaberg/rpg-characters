﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Characters
{
    public interface IXPObserver
    {
        public void OnLevelUp();
    }

    public class XPManager
    {
        // Keep track of observers
        private Dictionary<int, IXPObserver> observers = new Dictionary<int, IXPObserver>();
        private int lastObserverId = 0;

        private int currentXP = 0;
        private int level = 1;
        private int reqXPToLevelUp = 100;

        // On level up, the required XP to reach next level is increased by [reqXPMultiplier] %.
        private int reqXPMultiplier = 10;

        public int XPToLevelUp
        {
            get
            {
                return reqXPToLevelUp - currentXP;
            }
        }
        public int Level { get { return level; } }

        public int XP {
            get
            {
                return currentXP;
            }
        }

        public void GrantXP(int xp)
        {
            if (currentXP + xp >= reqXPToLevelUp)
            {
                int remainder = xp - reqXPToLevelUp;
                LevelUp();

                // Grant remainder - if we gained enough XP to level up multiple times, it'll be handled recurisively
                GrantXP(remainder);
            }
            else
                currentXP += xp;
        }

        public void LevelUp() {
            // We leveled up!
            level += 1;

            // Make it ever more tedious to level up next time.
            reqXPToLevelUp += reqXPToLevelUp / reqXPMultiplier;

            // Notify observers.
            foreach(var observer in observers)
            {
                observer.Value.OnLevelUp();
            }
        }

        public int Subscribe(IXPObserver observer)
        {
            int observerId = lastObserverId++;
            observers.Add(lastObserverId++, observer);
            return observerId;
        }

        public void Unsubscribe(int observerId)
        {
            if (!observers.ContainsKey(observerId)) return;

            observers.Remove(observerId);
        }
    }
}
