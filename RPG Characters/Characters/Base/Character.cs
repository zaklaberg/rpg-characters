﻿using System;
using System.Collections.Generic;
using RPG_Characters.Items.Weapons;
using RPG_Characters.Items.Armor;
using RPG_Characters.Abilities;

namespace RPG_Characters.Characters
{
    public abstract class Character 
        : IXPObserver
    {
        // A character starts with baseAttributes - these will never change
        protected CharacterAttributes baseAttributes = new CharacterAttributes();

        // A character may gain temporary attributes from (equipped) items
        protected CharacterAttributes itemAttributes = new CharacterAttributes();

        // A character may also be subject to various abilities(attacks, heals, buffs, debuffs)
        // that modify their attributes
        protected CharacterAttributes abilityAttributes = CharacterAttributes.Fill(0);

        // The set of abilities that this character can use
        public readonly List<IAbility> Abilities = new List<IAbility>();

        public CharacterAttributes Attributes { get { return baseAttributes + itemAttributes + abilityAttributes; } }

        // Equipped items
        public Weapon weapon = null;
        public Dictionary<ArmorSlot, Armor> armor = new Dictionary<ArmorSlot, Armor>();
        
        // XP handling. These are tightly coupled - if we want another XPManager, we're screwed (OCP)
        // But for now it seems a little pointless to do DI on such a simple class.. could do a default arg in constructor I guess
        protected XPManager xpManager = new XPManager();

        public void GrantXP(int xp) { xpManager.GrantXP(xp); }
        public int XPToLevelUp 
        {
            get
            {
                return xpManager.XPToLevelUp;
            }
        }
        public int Level
        {
            get { return xpManager.Level; }
        }

        public Character()
        {
            xpManager.Subscribe(this);

            // Everyone gets to do a normal attack
            Abilities.Add(new NormalAttack());
        }

        public void UnequipWeapon()
        {
            weapon = null;
        }

        public void UnequipArmor(ArmorSlot armorSlot)
        {
            if (!armor.ContainsKey(armorSlot))
                return;

            // Remove previously granted attributes from this item
            itemAttributes -= armor[armorSlot].ProvidedAttributes; 
            armor[armorSlot] = null;
        }

        public void Equip(Armor armor)
        {
            // If an armor was equipped, properly unequip it
            if (this.armor.ContainsKey(armor.Slot))
                UnequipArmor(armor.Slot);

            // Add new item attributes from armor
            itemAttributes += armor.ProvidedAttributes;
            this.armor[armor.Slot] = armor;
        }

        public void Equip(Weapon weapon)
        {
            if (this.weapon != null)
                UnequipWeapon();
            this.weapon = weapon;
        }
        
        public void UseAbility(IAbility ability, Character target)
        {
            if (!Abilities.Contains(ability))
                throw new Exception("This character hasn't learned that ability.");
            ability.Use(this, target);
        }

        public void TakeDamage(int damage)
        {
            abilityAttributes.HP -= damage;

            // Check for death, emit event & react etc.
        }

        // Implement to upgrade attributes, draw hooray messages and/or call your mother.
        // Automatically called by the XP Manager - a poorly paid and underappreciated job.
        public abstract void OnLevelUp();
    }
}
