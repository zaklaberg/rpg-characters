﻿using System;
using System.Collections.Generic;
using System.Text;

using RPG_Characters.Characters;

namespace RPG_Characters.Abilities
{
    public interface IAbility
    {
        public void Use(Character self, Character target);
    }
}
