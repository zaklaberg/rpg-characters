﻿using Xunit;
using RPG_Characters.Items.Armor;

namespace UnitTests.Items
{
    public class ArmorTests
    {
        [Fact]
        public void ClothAttributes()
        {
            Armor armor = Armor.Create(ArmorType.CLOTH)
                .ItemLevel(10)
                .Slot(ArmorSlot.LEGS)
                .Name("Cloth Leggings of the Magi")
                .Build();

            int[] expectedArmorAttrs = new int[] { 36, 6, 13 }; // HP, Dex, Int
            int[] actualArmorAttrs = new int[] { armor.ProvidedAttributes.HP, armor.ProvidedAttributes.Dexterity, armor.ProvidedAttributes.Intelligence };

            Assert.Equal(expectedArmorAttrs, actualArmorAttrs);
        }

        [Fact]
        public void PlateChestAttributes()
        {
            Armor armor = Armor.Create(ArmorType.PLATE)
                .ItemLevel(15)
                .Slot(ArmorSlot.BODY)
                .Name("Plate Chest of the Juggernaut")
                .Build();

            int[] expectedArmorAttrs = new int[] { 210, 33, 16 };
            int[] actualArmorAttrs = new int[] { armor.ProvidedAttributes.HP, armor.ProvidedAttributes.Strength, armor.ProvidedAttributes.Dexterity };

            Assert.Equal(expectedArmorAttrs, actualArmorAttrs);
        }
    }
}
