﻿using Xunit;
using RPG_Characters.Items.Weapons;

namespace UnitTests
{
    public class WeaponTests
    {
        [Fact]
        public void MagicWeaponDamage()
        {
            RPG_Characters.Items.Weapons.Weapon stinkyStaff = RPG_Characters.Items.Weapons.Weapon.Create(WeaponType.MAGIC).ItemLevel(3).Name("Longstaff of the Smelly Wizard").Build();
            Assert.Equal(31, stinkyStaff.Damage);
        }
        [Fact]
        public void RangedWeaponDamage()
        {
            RPG_Characters.Items.Weapons.Weapon wolfBow = RPG_Characters.Items.Weapons.Weapon.Create(WeaponType.RANGED).ItemLevel(10).Name("Long Bow of the Lone Wolf").Build();
            Assert.Equal(35, wolfBow.Damage);

        }
        [Fact]
        public void MeleeWeaponDamage()
        {
            RPG_Characters.Items.Weapons.Weapon greatAxe = RPG_Characters.Items.Weapons.Weapon.Create(WeaponType.MELEE).ItemLevel(5).Name("Great Axe of the Exiled").Build();
            Assert.Equal(25, greatAxe.Damage);
        }
    }
}
