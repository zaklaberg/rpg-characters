﻿using Xunit;
using RPG_Characters.Characters;
using RPG_Characters.Items.Armor;
using RPG_Characters.Items.Weapons;

namespace IntegrationTests
{
    public class NormalAttackOnCharacter
    {
        [Fact]
        public void TestWarriorNormalAttack()
        {
            // Build a warrior as the attacker
            Warrior warrior = new Warrior();
            while (warrior.Level < 9) warrior.GrantXP(30);

            Weapon greatAxe = Weapon.Create(WeaponType.MELEE)
                .ItemLevel(5)
                .Name("Great Axe of the Exiled")
                .Build();

            Armor armor = Armor.Create(ArmorType.PLATE)
                .ItemLevel(5)
                .Slot(ArmorSlot.BODY)
                .Name("Plate Chest of the Juggernaut")
                .Build();

            warrior.Equip(greatAxe);
            warrior.Equip(armor);

            // Build the dumbest mage in existence as the target
            Mage trump = new Mage();
            while (trump.Level < 15) trump.GrantXP(200);
            int trumpHpPriorToAssault = trump.Attributes.HP;

            // Warrior with the given equipment is expected to do this much damage
            int expectedDamage = 119;

            warrior.UseAbility(warrior.Abilities[0], trump);
            int trumpHpAfterAssault = trump.Attributes.HP;

            Assert.Equal(expectedDamage, trumpHpPriorToAssault - trumpHpAfterAssault);
        }
    }
}
