﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Characters.Items
{
    public abstract class Item
    {
        public int ItemLevel { get; protected set; } = 0;
        public string Name { get; protected set; } = null;
    }
}
