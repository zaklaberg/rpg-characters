﻿using System;
using System.Collections.Generic;

#nullable disable

namespace RPG_Characters.Model
{
    public partial class Character
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ClassId { get; set; }
        public int Level { get; set; }
        public int Xp { get; set; }
    }
}
