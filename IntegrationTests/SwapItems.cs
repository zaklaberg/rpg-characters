﻿using Xunit;
using RPG_Characters.Items.Armor;
using RPG_Characters.Characters;

namespace IntegrationTests
{
    public class SwapItemTests
    {
        [Fact]
        public void WarriorChestSwap()
        {
            Warrior warrior = new Warrior();
            while (warrior.Level < 9) warrior.GrantXP(30);

            Armor tunic = Armor.Create(ArmorType.CLOTH)
                .ItemLevel(1)
                .Slot(ArmorSlot.BODY)
                .Name("Cloth Tunic of the Depressed Scholar")
                .Build();

            Armor plate = Armor.Create(ArmorType.PLATE)
                .ItemLevel(25)
                .Slot(ArmorSlot.BODY)
                .Name("Plate Chest of the Juggernaut")
                .Build();

            int strPriorToEquip = warrior.Attributes.Strength;

            // Plate provides a bunch of strength. Tested in UnitTests/Items/Armor
            warrior.Equip(plate);

            // Tunic provides zero strength
            warrior.Equip(tunic);

            Assert.Equal(strPriorToEquip, warrior.Attributes.Strength);
        }
    }
}
