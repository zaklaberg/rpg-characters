﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Characters.Characters
{
    public class Warrior : Character
    {
        public Warrior()
        {
            baseAttributes.HP = 150;
            baseAttributes.Strength = 10;
            baseAttributes.Dexterity = 3;
            baseAttributes.Intelligence = 1;
        }
        
        public override void OnLevelUp()
        {
            baseAttributes.HP += 30;
            baseAttributes.Strength += 5;
            baseAttributes.Dexterity += 2;
            baseAttributes.Intelligence += 1;
        }
    }
}
